<?php

namespace Drupal\poster_slider;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a Slider entity.
 *
 * We have this interface so we can join the other interfaces it extends.
 *
 * @ingroup poster_slider
 */
interface SliderInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {
}